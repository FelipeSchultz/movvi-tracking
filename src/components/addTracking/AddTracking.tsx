import React, { ChangeEvent, useEffect, useState } from 'react';
import { PrimaryButton } from '../buttons/primaryButton/PrimaryButton';
import { TextField } from '../textField/TextField';
import { AddTrackingProps } from './AddTrackingProps';
import './AddTrackingStyle.scss'

export const AddTracking: React.FC<AddTrackingProps> = ({ onAdd }) => {
    const [cpf, setCpf] = useState('');
    const [nf, setNf] = useState('');

    const [buttonState, setButtonState] = useState(true);

    useEffect(() => setButtonState(!(cpf && nf)), [cpf, nf])

    const handleCpf = (event: ChangeEvent<HTMLInputElement>) => {
        setCpf(event.target.value)
    }

    const handleNf = (event: ChangeEvent<HTMLInputElement>) => {
        setNf(event.target.value)
    }

    const handleClick = () => {
        onAdd && onAdd(cpf, nf);
    }

    return (
        <div className='add-tracking'>
            <TextField placeholder='Digite o seu cpf' maxLength={11} onChange={handleCpf}></TextField>
            <TextField placeholder='Digite o numero da nota fiscal' onChange={handleNf}></TextField>

            <PrimaryButton onClick={handleClick} disabled={buttonState} title='Rastrear'></PrimaryButton>
        </div>
    );
}