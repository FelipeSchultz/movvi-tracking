export interface AddTrackingProps {
    onAdd?: (cpf: string, nf: string) => void;
}