import { ChangeEvent } from "react";

export interface TextFieldProps {
    value?: string,
    placeholder?: string,
    maxLength?: number;
    onChange?: (_: ChangeEvent<HTMLInputElement>) => any;
}