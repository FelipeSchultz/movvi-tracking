import React, { ChangeEvent, useState } from 'react';
import { TextFieldProps } from './TextFieldProps';
import './TextFieldStyle.scss';

export const TextField: React.FC<TextFieldProps> = ({ value, placeholder, maxLength, onChange }) => {
    const [inputValue, setInputValue] = useState<string>(value || '');

    const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
        const changeValue = event.target.value;

        setInputValue(changeValue);
        onChange && onChange(event);
    }


    return <input type="text" value={inputValue} placeholder={placeholder} onChange={handleChange} maxLength={maxLength} className='text-field'/>;
}