import { Track } from "../../services/interfaces/track";

export interface TrackingTableProps {
    track: Track;
}