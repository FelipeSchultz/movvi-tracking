import React from 'react';
import { TrackingTableProps } from './trackingTableProps';
import './TrackingTableStyle.scss'

const formatDate = (dateString: string) => {
    const date = dateString.substring(0, 10).split('-').reverse().join('/');
    const time = dateString.substring(11);

    return `${date} ${time}`;
}

export const TrackingTable: React.FC<TrackingTableProps> = ({ track }) => {
    return (
        <table className='tracking-table'>
            <thead>
                <tr>
                    <td>Data</td>
                    <td>Unidade</td>
                    <td>Status</td>
                </tr>
            </thead>

            <tbody>
                {
                    track.ocorrencias.map((event) => (
                        <tr key={event.data}>
                            <td>{formatDate(event.data)}</td>
                            <td>{event.unidade}</td>
                            <td>{event.descricao}</td>
                        </tr>
                    ))
                }
            </tbody>
        </table>
    );
}