import React, { useState } from 'react';
import { AccordionProps } from './accordionProps';
import './AccordionStyle.scss'

export const Accordion: React.FC<AccordionProps> = ({ title, children }) => {
    const [open, setOpen] = useState(true);

    const getOpenClass = () => open ? 'open' : '';

    return (
        <div className={`accordion ${getOpenClass()}`}>
            <header onClick={() => setOpen(!open)}>{title}</header>
            <section>
                {children}
            </section>
        </div>
    );
}