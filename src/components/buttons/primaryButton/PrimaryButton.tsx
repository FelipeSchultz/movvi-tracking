import React from 'react';
import { ButtonProps } from '../button-props';
import './PrimaryButtonStyle.scss'

export const PrimaryButton: React.FC<ButtonProps> = ({ title, disabled, onClick, type = 'button' }) => {
    return <button onClick={onClick} type={type} disabled={disabled} className='primary-button'>{title}</button>;
}