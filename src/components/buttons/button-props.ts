export interface ButtonProps {
    title: string;
    disabled?: boolean;
    onClick?: () => any;
    type?: 'button' | 'submit' | 'reset';
}