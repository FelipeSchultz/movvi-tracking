import React, { useState } from 'react';
import { Accordion } from '../../components/accordion/Accordion';
import { AddTracking } from '../../components/addTracking/AddTracking';
import { PrimaryButton } from '../../components/buttons/primaryButton/PrimaryButton';
import { TrackingTable } from '../../components/trackingTable/TrackingTable';
import { Track } from '../../services/interfaces/track';
import { store, getTrackingList, updateTracking } from '../../services/storageService';
import './TrackingStyle.scss';

export const TrackingPage: React.FC = () => {
    const [_, forceUpdate] = useState<Track[]>([]);
    const buildAccordion = () => {
        const trackList = getTrackingList();

        return trackList.map(track => (
            <Accordion title={`Código de rastreamento - ${track.conhecimento}`} key={track.conhecimento}>
                <TrackingTable track={track}></TrackingTable>
            </Accordion>
        ))
    }

    const handleStore = (cpf: string, nf: string) => {
        store(cpf, nf).then(forceUpdate);
    }

    const handleUpdate = () => {
        updateTracking()
        .then(() => {
            forceUpdate([])
        });


    }

    return (
        <div className='tracking-page'>
            <aside>
                <AddTracking onAdd={handleStore}></AddTracking>

                <div className="col-7">
                    <PrimaryButton title='Atualizar' onClick={handleUpdate}></PrimaryButton>
                </div>
            </aside>

            <aside>
                <div className="table-wrapper">
                    {buildAccordion()}
                </div>
            </aside>
        </div>
    );
}