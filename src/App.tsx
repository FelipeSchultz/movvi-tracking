import React from 'react';
import { TrackingPage } from './pages/trackingPage/TrackingPage';

function App() {
  return (
    <div className="App">
      <TrackingPage></TrackingPage>
    </div>
  );
}

export default App;
