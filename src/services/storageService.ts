import { Track } from "./interfaces/track";
import { trackProduct } from "./trackService";

export const store = async (cpf: string, nf: string) => {
    const data = await trackProduct(cpf, nf);

    data.cpf = cpf;

    save(data);

    return getTrackingList();
}

export const getTrackingList = (): Track[] => {
    const storage = localStorage.getItem('tracking');

    return storage ? JSON.parse(storage) : [];
}

export const updateTracking = async () => {
    const trackings = getTrackingList();

    const promises: Promise<Track>[] = [];

    trackings.forEach(track => {
        if (!track.cpf) {
            return;
        }

        promises.push(trackProduct(track.cpf, track.notas[0]))
    })

    const resolved = await Promise.allSettled(promises);

    const newTrackingList: Track[] = [];

    resolved.forEach(p => {
        if (p.status === 'fulfilled') {
            p.value.cpf = trackings.find(t => t.conhecimento === p.value.conhecimento)?.cpf;

            newTrackingList.push(p.value)
        }
    })

    if (newTrackingList.length) {
        localStorage.setItem('tracking', JSON.stringify(newTrackingList))
    }

}

const save = (track: Track) => {
    const storage = localStorage.getItem('tracking');

    if (!storage) {
        localStorage.setItem('tracking', JSON.stringify([track]))
        return;
    }

    const tracking: Track[] = JSON.parse(storage);

    const shouldAdd = tracking.find(t => t.conhecimento === track.conhecimento);

    if (!shouldAdd) {
        tracking.push(track);

        localStorage.setItem('tracking', JSON.stringify(tracking))
    }

}