import { SourceDestiny } from "./sourceDestiny";
import { TrackEvent } from "./trackEvent";

export interface Track {
    cpf?: string;
    conhecimento: string;
    notas: string[];
    ocorrencias: TrackEvent[];
    origemDestino: SourceDestiny[];
    prevEntrega: string;
}