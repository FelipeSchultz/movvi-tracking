import { Axios } from 'axios';
import { Track } from './interfaces/track';

const axios = new Axios({ baseURL: 'https://apimovvi.meridionalcargas.com.br/api/rastrear-carga' })

export const trackProduct = async (cpf: string, nf: string): Promise<Track> => {
    const { data, status } = await axios.get(`/${cpf}/${nf}`);

    if (status !== 200) {
        throw new Error('Não foi possível rastrear o objeto');
    }


    return JSON.parse(data);
}